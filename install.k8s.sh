#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Set the Kubernetes version as found in the UCP Dashboard or API
# k8sversion=v1.14.0

#osname=darwin or linux
osname=$(uname | tr '[:upper:]' '[:lower:]')

# Get the kubectl binary.
#curl -LO https://storage.googleapis.com/kubernetes-release/release/$k8sversion/bin/$osname/amd64/kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/$osname/amd64/kubectl

# Make the kubectl binary executable.
chmod +x ${DIR}/kubectl

# Move the kubectl executable to /usr/local/bin.
# sudo mv ${DIR}/kubectl /usr/local/bin/kubectl