#!/usr/bin/env bash

# curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | sudo bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
sudo ./get_helm.sh

helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

helm install 'nfs' 'stable/nfs-server-provisioner' --set=storageClass.name=nfs-client

kubectl rollout status statefulset/nfs-nfs-server-provisioner
kubectl get storageclass