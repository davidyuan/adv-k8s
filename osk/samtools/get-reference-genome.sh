#!/bin/bash

samtools

# /datasource/human_g1k_v37/human_g1k_v37.fasta
# /datasource/data/human_g1k_v37/human_g1k_v37.fasta

cat /datasource/human_g1k_v37/human_g1k_v37.fasta | \
  awk '/^>/ {if(N>0) printf("\n"); printf("%s\n",$0);++N;next;} { printf("%s",$0);} END {printf("\n");}' | \
  split -l 2 - seq_

for f in seq_a*
do
  j=`head -1 $f | tr -d '>' | awk '{ print $1 }'`
  echo $f $j
  cp $f human_g1k_v37.${j}.fasta
done

for f in human_g1k_v37.*.fasta; do samtools faidx $f; done

rm -f seq_*
