#!/usr/bin/env bash

curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash
helm init --upgrade

kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
kubectl rollout status deployment.v1.apps/tiller-deploy --namespace=kube-system

# https://github.com/kubernetes-incubator/external-storage/tree/master/nfs-client
helm install --name=nfs stable/nfs-client-provisioner --set nfs.server=10.130.78.90 --set nfs.path=/export
kubectl rollout status deployment.v1.apps/nfs-nfs-client-provisioner --timeout=60m