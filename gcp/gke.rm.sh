#!/usr/bin/env bash

project="extreme-lore-114513"
cluster="tsi-gke-1"
#region="europe-north1"
#zone="europe-north1-a"
zone="europe-west1-b"

#gcloud beta container clusters delete ${cluster} --region ${region} --project ${project} --quiet
gcloud beta container clusters delete ${cluster} --zone ${zone} --project ${project} --quiet