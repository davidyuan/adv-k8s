#!/usr/bin/env bash

# There are 26 fasta files in human reference genome v37: /datasource/human_g1k_v37/human_g1k_v37.${chromosome}.fasta: 1-22, MT, X, Y, GL000207.1
# query=/datasource/data/HG00125/alignment/HG00125.chrom20.ILLUMINA.bwa.GBR.low_coverage.20120522
date
#max_pods=$1

index=0
regions=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 MT X Y 'GL000207.1')

# Scale up to the number of pods desired
#dpmname=$(kubectl get deployment -o name | grep -m 1 freebayes | cut -d '/' -f2)
#kubectl autoscale deployment ${dpmname} --cpu-percent=50 --min=1 --max=${max_pods}
#kubectl get hpa

function nextpod () {
  local _result=$1

  local _podnames=( $(kubectl get pods --field-selector=status.phase=Running -o=jsonpath='{.items[?(@.metadata.labels.run=="freebayes-dpm")].metadata.name}') )
  local _num_of_pods=${#_podnames[@]}

  ((index++)); if [ ${index} -ge ${_num_of_pods} ]; then index=0; fi
  local _podname=${_podnames[${index}]}

#  echo "Pod names: ${_podnames[@]}."; echo "Next pod: ${_podname}."
  eval $_result="'${_podname}'"
}
nextpod podname; echo "Next pod: ${podname}."

# Run Samtools
#kubectl exec -i ${podname} -c samtools -- get-reference-genome.sh

# Copy alignments
for query in $(cat queries.in); do
  kubectl exec -i ${podname} -c freebayes -- get-alignment.sh ${query}&
  nextpod podname; echo "Next pod: ${podname}."
done
wait

# Run Freebayes
for query in $(cat queries.in); do
#  kubectl exec -i ${podname} -c freebayes -- get-alignment.sh ${query}

  query=/datasource/data/${query}
  kubectl exec -i ${podname} -c freebayes -- ls -l ${query}
  fname=$(echo ${query} | cut -d '/' -f6 | cut -d '.' -f -7)

  for region in ${regions[@]}; do
    region_ref=${region}

    kubectl exec -i ${podname} -c freebayes -- freebayes \
        --region ${region} \
        --fasta-reference /datasource/human_g1k_v37/human_g1k_v37.${region_ref}.fasta \
        --vcf /workspace/result/${fname}.${region}.vcf \
        ${query}&

    nextpod podname; echo "Next pod: ${podname}."
  done
  wait; date
done
