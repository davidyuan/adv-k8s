#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

kubectl version
kubectl cluster-info

kubectl delete -f ${DIR}/minio.yml
kubectl delete -f ${DIR}/freebayes.yml
kubectl delete -f ${DIR}/pvc-1000g.yml
kubectl delete -f ${DIR}/pvc-workspace.yml
kubectl delete hpa freebayes-dpm

kubectl get deployment
kubectl get pvc
kubectl get pv
kubectl get svc
kubectl get hpa