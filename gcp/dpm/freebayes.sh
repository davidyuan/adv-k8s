#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

kubectl version
kubectl cluster-info

kubectl apply -f ${DIR}/pvc-workspace.yml
kubectl apply -f ${DIR}/pvc-1000g.yml
kubectl apply -f ${DIR}/minio.yml
kubectl apply -f ${DIR}/freebayes.yml
kubectl rollout status deployment.v1.apps/freebayes-dpm --request-timeout=60m

kubectl get pv
kubectl get pvc
kubectl get deployment
kubectl get pod
kubectl get svc

podname=$(kubectl get pods -o name | grep -m 1 freebayes-dpm | cut -d '/' -f2)
kubectl exec -i ${podname} -c samtools -- get-reference-genome.sh
kubectl exec -i ${podname} -c freebayes -- run-freebayes.sh
