#!/usr/bin/env bash

index=0

function nextpod () {
  local _result=$1

  local _podnames=( $(kubectl get pods --field-selector=status.phase=Running -o=jsonpath='{.items[?(@.metadata.labels.run=="freebayes-dpm")].metadata.name}') )
  local _num_of_pods=${#_podnames[@]}

  ((index++)); if [ ${index} -ge ${_num_of_pods} ]; then index=0; fi
  local _podname=${_podnames[${index}]}

  echo "Pod names: ${_podnames[@]}."; echo "Next pod: ${_podname}."
  eval $_result="'${_podname}'"
}

for query in $(cat queries.in); do
  nextpod podname; echo "Next pod: ${podname}."
done