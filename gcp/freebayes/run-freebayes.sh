#!/bin/bash

freebayes

# Copy a query genome:
query=HG00125/alignment/HG00125.chrom20.ILLUMINA.bwa.GBR.low_coverage.20120522.bam
fname=$(echo ${query} | cut -d '/' -f 3 | cut -d '.' -f -7)

to_file=$(get-alignment.sh ${query})

# Pick only chromosome 20. Could run over all chromosomes (1-22,X,Y), but 20 is smallest
# N.B. If pick different region for reference fasta and for --region argument, the
# executable will end very quickly, since the query and reference won't overlap.
# This can be useful in testing!
region=20
region_ref=$region

# Run it!
    freebayes \
      --region $region \
      --fasta-reference /datasource/human_g1k_v37/human_g1k_v37.${region_ref}.fasta \
      --vcf /workspace/result/${fname}.${region}.vcf \
      ${to_file}
