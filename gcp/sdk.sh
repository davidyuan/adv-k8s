#!/usr/bin/env bash

# https://cloud.google.com/sdk/docs/downloads-versioned-archives
curl https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-258.0.0-linux-x86_64.tar.gz | tar -xz

google-cloud-sdk/install.sh --quiet --additional-components beta #kubectl

#gcloud init --console-only --skip-diagnostics

gcloud auth activate-service-account --key-file=vault/gcp/TSI-EBI-84ce96536735.json

