#!/usr/bin/env bash

project="extreme-lore-114513"
cluster="tsi-gke-1"
#region="europe-north1"
#zone="europe-north1-a"
zone="europe-west1-b"

# Create a cluster
#gcloud beta container --project ${project} clusters create ${cluster} --region ${region} --no-enable-basic-auth --cluster-version "1.13.5-gke.10" --machine-type "n1-standard-2" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --enable-cloud-logging --enable-cloud-monitoring --no-enable-ip-alias --network "projects/extreme-lore-114513/global/networks/default" --subnetwork "projects/extreme-lore-114513/regions/europe-north1/subnetworks/default" --enable-autoscaling --min-nodes "3" --max-nodes "20" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair --metadata disable-legacy-endpoints=true
gcloud beta container --project ${project} clusters create ${cluster} --zone ${zone} --no-enable-basic-auth --cluster-version "1.13.6-gke.5" --machine-type "n1-standard-4" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --enable-cloud-logging --enable-cloud-monitoring --no-enable-ip-alias --network "projects/extreme-lore-114513/global/networks/default" --subnetwork "projects/extreme-lore-114513/regions/europe-west1/subnetworks/default" --enable-autoscaling --min-nodes "3" --max-nodes "20" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair --metadata disable-legacy-endpoints=true
#gcloud beta container --project ${project} clusters create ${cluster} --zone ${zone} --no-enable-basic-auth --cluster-version "1.13.6-gke.5" --machine-type "n1-standard-4" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --enable-cloud-logging --enable-cloud-monitoring --no-enable-ip-alias --network "projects/extreme-lore-114513/global/networks/default" --subnetwork "projects/extreme-lore-114513/regions/europe-north1/subnetworks/default" --enable-autoscaling --min-nodes "3" --max-nodes "20" --addons HorizontalPodAutoscaling,HttpLoadBalancing,Istio --enable-autoupgrade --enable-autorepair --metadata disable-legacy-endpoints=true --istio-config auth=MTLS_PERMISSIVE

# Connect to a cluster
#gcloud beta container clusters get-credentials ${cluster} --region ${region} --project ${project}
gcloud beta container clusters get-credentials ${cluster} --zone ${zone} --project ${project}