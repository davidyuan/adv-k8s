#!/bin/bash

samtools

mkdir -p /datasource/human_g1k_v37/
cd /datasource/human_g1k_v37/

[ -f human_g1k_v37.fasta.gz ] || \
    wget --tries='inf' ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/human_g1k_v37.fasta.gz
gzip -dv human_g1k_v37.fasta.gz

cat /datasource/human_g1k_v37/human_g1k_v37.fasta | \
  awk '/^>/ {if(N>0) printf("\n"); printf("%s\n",$0);++N;next;} { printf("%s",$0);} END {printf("\n");}' | \
  split -l 2 - seq_

for f in seq_a*
do
  j=`head -1 $f | tr -d '>' | awk '{ print $1 }'`
  echo $f $j
  cp $f human_g1k_v37.${j}.fasta
done

for f in human_g1k_v37.*.fasta; do samtools faidx $f; done

rm -f seq_*
